package com.example.birat.major_project;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface FileService {
    @Multipart
    @POST("upload")
    Call<Image> upload(@Part MultipartBody.Part image);

    @GET("predict")
    Call<String> predict();
}
