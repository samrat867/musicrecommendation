package com.example.birat.major_project;

public class Image {

    private String status;

    public Image() { }

    public Image(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
