package com.example.birat.major_project;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmotionActivity extends AppCompatActivity {

    public FirebaseFirestore mFirestore; //for receiving emotion from firebase
    public static final String FIRE_LOG = "Fire_log";

    public String emotion, hola;

    private DatabaseReference mDatabase;

    private Button emoBtn, PlBtn;

    FileService fileService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emotion);

        emoBtn = (Button) findViewById(R.id.emoBtn);
        PlBtn = (Button) findViewById(R.id.playB);

//        mFirestore = FirebaseFirestore.getInstance();
//        mDatabase = FirebaseDatabase.getInstance().getReference();

        fileService = APIUtils.getFileService();
        emoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(EmotionActivity.this, "Fetching Emotion...", Toast.LENGTH_LONG).show();

                //delay for 10seconds

                Call<String> call = fileService.predict();
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            if (response.body() == null || response.body().equals("") || response.body().equalsIgnoreCase(" ")) {
                                Toast.makeText(EmotionActivity.this, "Cannot predict. Please click new photo or select new image.", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(EmotionActivity.this, "Prediction:  " + response.body(), Toast.LENGTH_SHORT).show();
                                emotion = response.body().toLowerCase().trim();
                                hola = response.body().toLowerCase().trim();
                            }
                        } else if (response.code() != 200) {
                            Toast.makeText(EmotionActivity.this, "Wrong Status Code: " + response.code(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        t.printStackTrace();
                        Toast.makeText(EmotionActivity.this, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        PlBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(EmotionActivity.this, hola.toUpperCase(), Toast.LENGTH_LONG).show();

                if (hola.equals("happy")) {
                    Intent happyAct = new Intent(EmotionActivity.this, happy.class);
                    startActivity(happyAct);

                }
                if (hola.equals("angry")) {
                    Intent angerAct = new Intent(EmotionActivity.this, angry.class);
                    startActivity(angerAct);

                }
                if (hola.equals("disgust")) {
                    Intent disAct = new Intent(EmotionActivity.this, disgustActivity.class);
                    startActivity(disAct);

                }
                if (hola.equals("fear")) {
                    Intent feAct = new Intent(EmotionActivity.this, love.class);
                    startActivity(feAct);

                }
                if (hola.equals("sad")) {
                    Intent sadAct = new Intent(EmotionActivity.this, sad.class);
                    startActivity(sadAct);

                }
                if (hola.equals("surprise")) {
                    Intent surpAct = new Intent(EmotionActivity.this, SurpriseActivity.class);
                    startActivity(surpAct);

                }
                if (hola.equals("neutral")) {
                    Intent neutralAct = new Intent(EmotionActivity.this, NeutralActivity.class);
                    startActivity(neutralAct);

                }
            }

        });

    }
}
