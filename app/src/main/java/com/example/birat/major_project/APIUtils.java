package com.example.birat.major_project;

public class APIUtils {
    private APIUtils(){}

    private  static final String API_URL = "https://emotion-detector-samrat.herokuapp.com/";

    public static FileService getFileService(){
        return RetrofitClient.getRetrofitClient(API_URL).create(FileService.class);
    }
}
